#!/usr/bin/env python

import max7219.led as led
import time
import twitter
from max7219.font import proportional, SINCLAIR_FONT, TINY_FONT, CP437_FONT
from random import randrange

device = led.matrix(cascaded=1)

device.show_message("MAX7219 LED Matrix Demo", font=proportional(CP437_FONT))

alejo = {}
execfile("twitter-friends.py", alejo)
time.sleep(1)
device.show_message(alejo["followers"])

time.sleep(1)
device.letter(0, ord('A'))
time.sleep(1)
for _ in range(5):
    for intensity in xrange(16):
        device.brightness(intensity)
        time.sleep(0.1)

device.brightness(7)

device.orientation(0)

time.sleep(1)
device.letter(0, ord('A'))
time.sleep(1)
for _ in range(10):
    device.invert(1)
    time.sleep(0.25)
    device.invert(0)
    time.sleep(0.25)

time.sleep(1)
device.show_message("alejodorowsky:", font=SINCLAIR_FONT)

time.sleep(1)
device.show_message(alejo["followers"], font=SINCLAIR_FONT)

time.sleep(1)
device.show_message(alejo["followers"], font=proportional(SINCLAIR_FONT))

# http://www.squaregear.net/fonts/tiny.shtml
time.sleep(1)
device.show_message(alejo["followers"],
font=proportional(TINY_FONT))

time.sleep(1)
for x in range(256):
#    device.letter(1, 32 + (x % 64))
    device.letter(0, x)
    time.sleep(0.1)

time.sleep(1)
device.show_message(alejo["followers"])

while True:
    for x in range(500):
        device.pixel(4, 4, 1, redraw=False)
        direction = randrange(8)
        if direction == 7 or direction == 0 or direction == 1:
            device.scroll_up(redraw=False)
        if direction == 1 or direction == 2 or direction == 3:
            device.scroll_right(redraw=False)
        if direction == 3 or direction == 4 or direction == 5:
            device.scroll_down(redraw=False)
        if direction == 5 or direction == 6 or direction == 7:
            device.scroll_left(redraw=False)

        device.flush()
        time.sleep(0.01)
